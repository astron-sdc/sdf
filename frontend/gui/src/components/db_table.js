import React from 'react';
import axios from 'axios';
import { Table } from 'reactstrap';

export default class DataBaseTable extends React.Component {
    // Define the constructor
    constructor(props) {
        super(props);
        this.state = {
            queries: []
        }
    }
    
    // Load the table when the page loads
    componentDidMount = () => {
        axios.get('http://sdc.astron.nl:5556/api/table/',{
        })
        .then( res => {
            this.setState({
                queries: res.data
            })
        })
        .catch( error => {
            console.log(error)
        })
    }
    
    renderTableBody() {
        return this.state.queries.map((query, index) => {
            const {ID, Project, Target, Calibrator, Status } = query
            return (
                <tr key={ID}>
                    <td>{ID}</td>
                    <td>{Project}</td>
                    <td>{Target}</td>
                    <td>{Calibrator}</td>
                    <td>{Status}</td>
                </tr>
            )
        })
    }
    
    // Create the layout
    render () {
        return (
            <Table striped>
              <thead>
              <tr>
                <th>ID</th>
                <th>Project</th>
                <th>Target</th>
                <th>Calibrator</th>
                <th>Status</th>
              </tr>
              </thead>
              <tbody>
                 {this.renderTableBody()}
              </tbody>
            </Table>
        )
    }
}
