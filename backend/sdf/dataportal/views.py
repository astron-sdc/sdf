from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from .serializers import LofarLTASerializer
from .utils.process_user_request import process_user_request
from .utils.query_database import get_processing_status, get_dbtable_entries
from .models import LofarLTAObservation


class observation_list(ModelViewSet):
    queryset = LofarLTAObservation.objects.all()
    serializer_class = LofarLTASerializer
    filterset_fields = ['createDate', 'dataProductType', 'duration', 'dataProductIdentifier', 'filename']

    def put(self, request, *args, **kwargs):
        result = process_user_request(request.data)
        return Response(result)

@api_view(['GET'])
def queue_status(request):
    if request.method == 'GET':
        # Query the database and get info
        processed_queries, total_queries = get_processing_status()
        results = {
            'processed_queries': processed_queries,
            'total_queries'    : total_queries
        }
        return Response(results)

@api_view(['GET'])
def queue_table(request):
    if request.method == 'GET':
        # Query the database and get info
        results = get_dbtable_entries()
        return Response(results)
