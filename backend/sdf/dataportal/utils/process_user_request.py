"""This module contains all the backend functions needed to handle the request
   a user makes through the web interface."""

from datetime import datetime
from django.core.exceptions import ValidationError
from django.core.validators import EmailValidator
from dataportal.models import UserRequests

def __parse_srm(srm):
    """
    For a given srm file, parse the srm and
    return the project code, filename of the MS, and SAS ID.
    """
    split_list = srm.split('/')
    project_code = [s for s in split_list if 'lc' in s][0]
    idx = split_list.index(project_code)
    sasid = split_list[idx+1]
    filename = split_list[idx+2]
    str_replace = '_' + filename.split('_')[-1]
    filename = filename.replace(str_replace, '')
    return project_code, filename, sasid

def is_valid_user_name(user_name):
    """
    Is the given username valid?
    Return True if valid. Else False.
    """
    if user_name is '':
        return False
    return True

def is_valid_user_email(user_email):
    """
    Is the given email valid?
    Return True if valid. Else False.
    """
    validator = EmailValidator()
    try:
        validator(user_email)
    except ValidationError:
        return False
    return True

def is_valid_demix_setup(demix_src):
    """
    Is the demix setup valid?
    Return True if valid. Else False.
    """
    if demix_src.count('A') > 2:
        return False
    return True

def enough_cal_and_target_srms(cal_list, tar_list):
    """
    Check if enough calibrator and target srms are specified. Assumes all items 
    in both the list are valid srms.
    Return True if yes. Else False.
    """
    # Remove duplicate entries if any
    cal_list = set(cal_list)
    tar_list = set(tar_list)
    if len(cal_list) < 40 or len(tar_list) < 10:
        return False
    return True

def __get_info_from_lta(srm):
    """
    For a given srm file, query the LOFAR LTA for some useful information.
    Returns a dictionary containing the following keys:
     - central freq, channels per subband, time resolution, duration,
       field, start time, sas id, process identifier
    """
    # Django expects the awlofar imports inside the function
    # Otherwise, database connection does not work.
    from awlofar.database.Context import context
    from awlofar.main.aweimports import CorrelatedDataProduct
    from awlofar.config.Environment import Env
    # Parse the srm string get info
    project_code, filename, sasid = __parse_srm(srm)
    # Query the LTA using this information
    context.set_project(project_code.upper())
    query = CorrelatedDataProduct.select_all()
    query &= query.project_only(project_code.upper())
    query &= CorrelatedDataProduct.dataProductIdentifierName == filename

    if len(query) != 1:
        # TODO: There is something wrong with the query. Throw error
        pass

    # Extract all the required information from this query and
    # compile it as a python dict
    info = {
        'central freq'        : query[0].centralFrequency,
        'channels per subband': query[0].channelsPerSubband,
        'time resolution'     : query[0].integrationInterval,
        'duration'            : query[0].duration,
        'field'               : query[0].subArrayPointingIdentifierName,
        'start time'          : query[0].startTime,
        'sasid'               : sasid,
        'project code'        : project_code,
        'process identifier'  : query[0].processIdentifier
    }
    return info

def __is_srm_valid(srm_list):
    """
    Check if the srms in srm_list are valid.
    Return True if yes. Else False.
    """
    # Find the process identifier
    info = __get_info_from_lta(srm_list[0])
    
    # Django expects the awlofar imports inside the function
    # Otherwise, database connection does not work.
    from awlofar.database.Context import context
    from awlofar.main.aweimports import CorrelatedDataProduct
    from awlofar.config.Environment import Env
    # Parse the srm string get info
    project_code, filename, sasid = __parse_srm(srm_list[0])
    # Query the LTA using this information
    context.set_project(project_code.upper())
    query = CorrelatedDataProduct.select_all()
    query &= query.project_only(project_code.upper())
    query &= CorrelatedDataProduct.processIdentifier == info['process identifier']
    
    valid_file_names = []
    for q in query:
        valid_file_names.append(q.filename)
    
    for srm in srm_list:
        filename = srm.split('/')[-1].rstrip()
        if filename not in valid_file_names:
            return False

    return True

def __is_cal_wide(cal_srm_beg, cal_srm_end, target_srm_beg, target_srm_end):
    """
    Check if the selected calibrator subbands is at least as wide as the target
    subband list. This ensure that prefactor does not have to extrapolate the
    calibrator solutions.
    Params:
        cal_srm_beg: srm of the first calibrator subband
        cal_srm_end: srm of the last calibrator subband
        target_srm_beg: srm of the first target subband
        target_srm_end: srm of the last target subband
    Returns:
       False, if at least one target subband is outside the calibrator subband range.
       True, otherwise
    """
    cal_beg_freq = __get_info_from_lta(cal_srm_beg)['central freq']
    cal_end_freq = __get_info_from_lta(cal_srm_end)['central freq']
    target_beg_freq = __get_info_from_lta(target_srm_beg)['central freq']
    target_end_freq = __get_info_from_lta(target_srm_end)['central freq']
    is_fail = True
    if target_beg_freq < cal_beg_freq or target_end_freq > cal_end_freq:
        is_fail = False
    return is_fail

def __insert_into_database(param_to_db):
    """
    Insert the user request into the database
    """
    request_entry = UserRequests(
        bad_baselines = param_to_db['bad_baselines'],
        request_datetime_utc = datetime.utcnow(),
        calibrator_date = param_to_db['calibrator_date'],
        calibrator_dt = param_to_db['calibrator_dt'],
        calibrator_id = param_to_db['calibrator_id'],
        calibrator_name = param_to_db['calibrator_name'],
        calibrator_nchan = param_to_db['calibrator_nchan'],
        calibrator_nsb = param_to_db['calibrator_nsb'],
        target_date = param_to_db['target_date'],
        dt = param_to_db['dt'],
        field = param_to_db['field'],
        target_id = param_to_db['target_id'],
        integration = param_to_db['integration'],
        location = param_to_db['location'],
        nchan = param_to_db['nchan'],
        nsb = param_to_db['nsb'],
        priority = param_to_db['priority'],
        project_code = param_to_db['project_code'],
        status = param_to_db['status'],
        time_avg = param_to_db['time_avg'],
        freq_avg = param_to_db['freq_avg'],
        demix_src = param_to_db['demix_src'],
        skymodel = param_to_db['skymodel'],
        cal_srm = param_to_db['cal_srm'],
        target_srm = param_to_db['target_srm'],
        u_name = param_to_db['u_name'],
        u_email = param_to_db['u_email'],
        raw_data = param_to_db['raw_data'],
        prefactor_ver = param_to_db['prefactor_ver']
    )
    request_entry.save()

def process_user_request(query):
    """
    Parse the axios request from the frontend and do the needful.
    """
    import time
    start = time.time()
    # Parse the query and get the relevant parameters
    calSRMList = query['params']['calSRMList'].split('\n')
    tarSRMList = query['params']['tarSRMList'].split('\n')
    # Remove empty items in the list that were created due to empty lines
    calSRMList = list(filter(None, calSRMList))
    tarSRMList = list(filter(None, tarSRMList))
    # Parse the remaining queries
    user_name = query['params']['userName']
    user_email = query['params']['userEmail']
    bad_baselines = query['params']['badBaselines']
    tAvg = query['params']['tAvg']
    fAvg = query['params']['fAvg']
    skyModel = query['params']['skyModel']
    isCygA = query['params']['isCygA']
    isCasA = query['params']['isCasA']
    isTauA = query['params']['isTauA']
    isVirA = query['params']['isVirA']
    demix_src = ''
    if isCygA: demix_src += 'CygA,'
    if isCasA: demix_src += 'CasA,'
    if isTauA: demix_src += 'TauA,'
    if isVirA: demix_src += 'VirA'
    
    # Check if an identical request exists in the database
    u = UserRequests.objects.filter(
            cal_srm=calSRMList,
            target_srm=tarSRMList,
            bad_baselines=bad_baselines,
            u_name=user_name,
            u_email=user_email,
            demix_src=demix_src,
            time_avg=int(tAvg),
            freq_avg=int(fAvg),
            skymodel=skyModel
    )
    if len(u) > 0:
        result = {
            'is_error': True,
            'msg'     : 'You have made the same request before!'
        }
        return result    

    # Is the user name valid
    if not is_valid_user_name(user_name):
        result = {
            'is_error': True,
            'msg'     : 'User name cannot be empty.'
        }
    # Is the email ID valid?
    elif not is_valid_user_email(user_email):
        result = {
            'is_error': True,
            'msg'     : 'Enter a valid email address.'
        }
    # Validate demix sources
    elif not is_valid_demix_setup(demix_src):
        result = {
            'is_error': True,
            'msg'     : 'Cannot demix more than 2 sources.'
        }
    # Check if all the calibrator srms are valid
    elif not __is_srm_valid(calSRMList):
        result = {
            'is_error': True,
            'msg'     : 'Calibrator SRM list contains at least one invalid entry.'
        }
    # Check if all the target srms are valid
    elif not __is_srm_valid(tarSRMList):
        result = {
            'is_error': True,
            'msg'     : 'Target SRM list contains at least one invalid entry.'
        }
    # Are there enough calibrator and targets in the srmlists?
    elif not enough_cal_and_target_srms(calSRMList, tarSRMList):
        result = {
            'is_error': True,
            'msg'     : 'Not enough calibrator and target srms specified.' + \
                        'A prefactor run needs at least 40 calibrators and ' + \
                        '10 target measurement sets.'
        }
    # Check if the calibrator bandwidth is wider than the target bandwidth
    elif not __is_cal_wide(calSRMList[0], calSRMList[-1], 
                           tarSRMList[0], tarSRMList[-1]):
        result = {
            'is_error': True,
            'msg'     : 'At least one target measurement set is outside ' + \
                        'frequency range spanned by the calibrator measurement sets.'
        }
    else:
        # If all validations are done
        # Gather all necessary input fields into a dict
        cal_info = __get_info_from_lta(calSRMList[0])
        target_info = __get_info_from_lta(tarSRMList[0])
        # Compile the dictionary that can be fed to the database
        param_to_db = {
            'bad_baselines'   : bad_baselines,
            'calibrator_date' : cal_info['start time'],
            'calibrator_dt'   : cal_info['time resolution'],
            'calibrator_id'   : int(cal_info['sasid']),
            'calibrator_name' : cal_info['field'],
            'calibrator_nchan': int(cal_info['channels per subband']),
            'calibrator_nsb'  : len(calSRMList),
            'target_date'     : target_info['start time'],
            'dt'              : target_info['time resolution'],
            'field'           : target_info['field'],
            'target_id'       : int(target_info['sasid']),
            'integration'     : target_info['duration'],
            'location'        : '',
            'nchan'           : int(target_info['channels per subband']),
            'nsb'             : int(len(tarSRMList)),
            'priority'        : int(0),
            'project_code'    : target_info['project code'],
            'status'          : '',
            'time_avg'        : int(tAvg),
            'freq_avg'        : int(fAvg),
            'demix_src'       : demix_src,
            'skymodel'        : skyModel,
            'cal_srm'         : calSRMList,
            'target_srm'      : tarSRMList,
            'u_name'          : user_name,
            'u_email'         : user_email,
            'raw_data'        : False,
            'prefactor_ver'   : ''
        }
        # Ingest the key pairs in param_to_db to database
        __insert_into_database(param_to_db)
        result = {
            'is_error': False,
            'msg'     : 'Your request has been submitted. ' +\
                        'You will receive an email when the data are ready.'
        }
    
    end = time.time()
    print(end-start)
    
    return result
