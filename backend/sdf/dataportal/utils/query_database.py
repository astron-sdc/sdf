"""
This module contains all the functions necessary to query the UserRequests 
database.
"""

from dataportal.models import UserRequests

def get_processing_status():
    """
    Query the UserRequests database and return the processed and total queries 
    in the database
    """
    query = UserRequests.objects.all()
    total_requests = len(query)
    query = UserRequests.objects.filter(status='')
    processed_requests = total_requests - len(query)
    return processed_requests, total_requests

def get_dbtable_entries():
    """
    Query the UserRequests database and return the last 5 unprocessed entries 
    in the database. 
    """
    MAX_ENTRIES = 5
    # Note that the frontend expects a list of dictionaries 
    # with the dict keys defined in renderTableBody() in db_table.js
    query_results = UserRequests.objects.filter(status='')\
                    .order_by('request_datetime_utc')
    if len(query_results) > MAX_ENTRIES:
        # If the database has more entries than what we want, 
        # get the last few entries
        n_queries = len(query_results)
        query_results = query_results[n_queries-MAX_ENTRIES:n_queries]
    if len(query_results) == 0:
        # Database is empty. Return an empty set
        result = []
    else:
        # Return all the entries in the query
        result = []
        index = 1
        for query in query_results:
            temp_dict = {
                'ID': index,
                'Project': query.project_code,
                'Target': query.field,
                'Calibrator': query.calibrator_name,
                'Status': query.status,
            }
            index += 1
            result.append(temp_dict)
    return result
