from rest_framework import serializers
from .models import LofarLTAObservation


class LofarLTASerializer(serializers.ModelSerializer):
    class Meta:
        model = LofarLTAObservation
        fields = '__all__'
