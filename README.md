# ASTRON Science Delivery Framework



## For Developers

### Deployment

Prerequisition:
* git
* python3
* npm

```bash
# setup a directory as install directory, e.g. $HOME
installdir=$HOME
# Clone the repository into install directory
cd $installdir
git clone git@gitlab.com:astron-sdc/sdf.git
cd sdf/backend
# create virtual environment
python3 -m venv env
# activate virtual environment
source env/bin/activate
# install all packages needed
pip install -r requirements.txt

# Install LOFAR LTA Client as described on the LOFAR wiki 
# See https://www.astron.nl/lofarwiki/doku.php?id=lta:client_installation
# Make sure you update your ${HOME}/.awe/Environment.cfg file
# The LTA Client installation step will create a new directory called
# instantclient_11_2 under lib/ . Include this directory under LD_LIBRARY_PATH
# Also, make sure to install the Oracle Client library (on Ubuntu, it is libaio1). 

cd ${installdir}
wget https://lta.lofar.eu/software/lofar_lta-2.7.1.tar.gz
tar xzvf lofar_lta-2.7.1.tar.gz
cd lofar_lta-2.7.1
python setup.py install_oracle
python setup.py install
cd ${installdir}/sdf/backend
export LD_LIBRARY_PATH=${PWD}/env/lib/instantclient_11_2:${LD_LIBRARY_PATH}

cd sdf
python manage.py makemigrations
python manage.py migrate --run-syncdb

python manage.py runserver 8080

cd $installdir
cd sdf/frontend/gui
npm install --save-dev
npm start
```

# Deploy on Nico's machine
1. SSH to sdf@dop457. Ask Sarrvesh for the password.
2. Navigate to /data/SDF/
3. Clone the SDF repository with 
`git clone https://gitlab.com/astron-sdc/sdf.git`
`cd sdf`
4. Build docker image with 
`docker build -t sdf:latest .`
5. 

# FAQ
* If you receive error `no such table: dataportal_userrequests`, delete everything under `dataportal/migrations` before running `makemigrations`.
