FROM ubuntu:18.04

# Install common dependencies
RUN apt-get update \
    && apt-get upgrade --yes \
    && apt-get install --yes curl gnupg \
    && curl -sSL https://deb.nodesource.com/gpgkey/nodesource.gpg.key | apt-key add - \
    && echo "deb https://deb.nodesource.com/node_12.x bionic main" | tee /etc/apt/sources.list.d/nodesource.list \
    && echo "deb-src https://deb.nodesource.com/node_12.x bionic main" | tee -a /etc/apt/sources.list.d/nodesource.list \
    && apt-get update
    
RUN apt-get --yes install --no-install-recommends \
    build-essential \
    git \
    libaio1 \
    libssl-dev \
    libmysqlclient-dev \
    nginx \
    nodejs \
    python3 \
    python3-dev \
    python3-pip \
    python3-setuptools \
    wget

# Update npm
RUN npm install -g n && n stable && pip3 install gunicorn

# Install backend under /opt
RUN cd /opt \
    && mkdir sdf && cd sdf 
COPY backend /opt/sdf/backend 
COPY frontend /opt/sdf/frontend
RUN wget https://lta.lofar.eu/software/lofar_lta-2.7.1.tar.gz \
    && tar -xvf lofar_lta-2.7.1.tar.gz \
    && cd lofar_lta-2.7.1/ \
    && python3 setup.py install_oracle --prefix=/opt/sdf/ \
    && python3 setup.py install --prefix=/opt/sdf 
    
ENV PATH=/usr/local/bin:$PATH

RUN cd /opt/sdf/backend \
    && pip3 install "wheel==0.34.2" \
    && pip3 install -r requirements.txt \
    && cd sdf \
    && python3 manage.py collectstatic \
    && mkdir -p /opt/sdf/site \ 
    && cp -r static /opt/sdf/site
    
RUN cd /opt/sdf/frontend/gui/ \
    && npm install \
    && npm run build \ 
    && cp -r build/* /opt/sdf/site
    
COPY conf/sdf.conf /etc/nginx/conf.d/
RUN rm /etc/nginx/sites-enabled/default && \
    ln -sf /dev/stdout /var/log/nginx/access.log && \
    ln -sf /dev/stderr /var/log/nginx/error.log

COPY conf/entrypoint.sh /usr/local/bin/update_db.sh
RUN chmod uga+x /usr/local/bin/update_db.sh

    
ENV PYTHONPATH=/opt/sdf/lib/python3.6/site-packages:/opt/sdf/backend/sdf
ENV LD_LIBRARY_PATH=/opt/sdf/lib/instantclient_11_2

RUN useradd -m sdf 
USER sdf
WORKDIR /home/sdf
